#!/bin/sh

OUTPUT=$(cat ~/Document/quit | dmenu -i)



if [ "$OUTPUT" = "Lock " ]; then
  lockscreen.sh
fi


if [ "$OUTPUT" = "Shutdown " ]; then
  sudo poweroff
fi

if [ "$OUTPUT" = "Quit " ]; then
  killall dwm
fi
